from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
import json

response = {"author":"Aldi Firmansyah"}
csui_helper = CSUIhelper()

def index(request):
	auth = csui_helper.instance.get_auth_param_dict()
	mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

	friend_list = Friend.objects.all()
	page = request.GET.get('page', 1)

	paginate_data = paginate_page(page,mahasiswa_list)

	mahasiswa = paginate_data['data']
	page_range = paginate_data['page_range']
	response['page_range'] = page_range
	response['mahasiswa_list'] = mahasiswa
	response['friend_list'] = friend_list
	response['auth'] = auth
	html = 'lab_7/lab_7.html'
	return render(request, html, response)

def friend_list(request):
	friend_list = Friend.objects.all()
	response['friend_list'] = friend_list
	html = 'lab_7/friend_list.html'
	return render(request, html, response)


def get_friend_list(request):
	if request.method == 'GET':
		friend_list = Friend.objects.all()
		data = serializers.serialize('json', friend_list)
		return HttpResponse(data)

@csrf_exempt
def add_friend(request):
	if request.method == 'POST':
		name = request.POST['name']
		npm = request.POST['npm']
		friend = Friend.objects.filter(npm__iexact=npm).exists()
		if (not friend):
			newFriend = Friend(friend_name=name,npm=npm)
			newFriend.save()
			
		data = model_to_dict(newFriend)
		return HttpResponse(data)


def delete_friend(request, friend_id):
	Friend.objects.filter(id=friend_id).delete()
	return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm(request):
	npm = request.POST.get('npm', None)
	data = {
		'is_taken': Friend.objects.filter(npm=npm).exists()
	}
	return JsonResponse(data)

def paginate_page(page, data_list):
	paginator = Paginator(data_list, 10)

	try:
		data = paginator.page(page)
	except PageNotAnInteger:
		data = paginator.page(1)
	except EmptyPage:
		data = paginator.page(paginator.num_pages)

	start_index = data.start_index()
	end_index = data.end_index()
	page_range = list(paginator.page_range)[start_index:end_index]
	paginate_data = {'data':data, 'page_range':page_range}
	return paginate_data

def model_to_dict(obj):
	data = serializers.serialize('json', [obj, ])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data