from django.db import models
from django.db import models

# Create your models here.
class Diary(models.Model):
	date = models.DateTimeField()
	activity = models.TextField(max_length=60)

class Person(models.Model):
	name = models.CharField(max_length=30)